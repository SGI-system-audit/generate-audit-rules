#!/bin/bash

# Find all the file systems that are locally mounted.
# And then file files with permission 4000 or 2000
# that is binary that, when run get a new userid, or a new groupid. those are use mostly for
#   - system related task
#   - database (e.g. file belong to oracle or mysql)
#   - 'hacking' tools ...

for i in `/bin/egrep '(ext4|ext3|ext2)' /etc/fstab | /bin/awk '{print $2}'`
do
  # Find all the files on the file system found above and print out
  #  and audit rule for it
  /usr/bin/find $i -xdev -type f \( -perm -4000 -o -perm 2000 \) -print 
  #| \
  #/bin/awk '{ print "-a always,exit -F path="  $1 " -F perm=x \
  #-F auid>500 -F auid!=-1 -k privileged -k ids-exec-high" }'
done
