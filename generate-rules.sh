
#!/bin/bash

while IFS='$\n' read -r line; do
  #echo ">>> $line"
  /bin/awk '{ print "-a always,exit -F path="  $line " -F perm=x \
  -F auid>500 -F auid!=-1 -k privileged -k ids-exec-high" }'
done

